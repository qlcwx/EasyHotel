﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMSOFT.EasyHotel.Common
{
    public class CommonConsts
    {
        public const string PleaseChoose="请选择";
        public const string Zero = "0";
        public const string HotelManager="店长";
        public const string MyDesKey = "d@1$#A!*";
        public const string FreeStatus = "空闲";
    }
}
